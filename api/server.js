const express = require('express');
const expressWs = require('express-ws');
const cors = require('cors');
const nanoid = require('nanoid');

const app = express();

const port = 8000;

expressWs(app);
app.use(cors());

const activeConnections = {};


app.ws('/drawing', (ws, req) => {
  const id = nanoid();
  console.log('client connected id=', id);
  activeConnections[id] = ws;

  ws.on('message', msg => {
    let decodedMessage;

    try{
      decodedMessage = JSON.parse(msg);
    } catch(e) {
      return console.log('Not a valid message')
    }

    switch(decodedMessage.type) {
      case 'CREATE_COORDINATES':
        Object.keys(activeConnections).forEach(connId => {
          const conn = activeConnections[connId];
          conn.send(JSON.stringify({
            type: 'NEW_COORDINATES',
            coordinates: decodedMessage.coordinates,
          }))
        });

        break;
      default:
        console.log('Not valid message type, ', decodedMessage.type);
    }

  });

  ws.on('close', msg => {
    console.log('client disconnected id=', id);
    delete activeConnections[id];
  });
});


app.listen(port, () => {
  console.log('server start on ' + port)
});