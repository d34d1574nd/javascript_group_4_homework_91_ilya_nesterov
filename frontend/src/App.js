import React, {Component, createRef} from 'react';
import './App.css';

class App extends Component {
  canvas = createRef();

  state = {
    coordinates: []
  };

  componentDidMount() {
    this.websocket = new WebSocket('ws://localhost:8000/drawing');

    this.websocket.onmessage = event => {
      const decodedMessage = JSON.parse(event.data);
      if (decodedMessage.type === 'NEW_COORDINATES') {
        this.setState({
          coordinates: [
            ...this.state.coordinates,
            decodedMessage.coordinates
          ]
        })
      }
    }
  }

  drawCircle = event => {
    const x = event.nativeEvent.offsetX;
    const y = event.nativeEvent.offsetY;

      this.websocket.send(JSON.stringify({
        type: 'CREATE_COORDINATES',
        coordinates: {x: x, y: y, coordinates: Math.random() * 25, startAngle: Math.PI/10, endAngle: -Math.PI/10}
      }));
  };

  render () {
    this.state.coordinates.forEach(coordinates => {
      const ctx = this.canvas.current.getContext('2d');
      ctx.beginPath();
      ctx.arc(coordinates.x, coordinates.y, coordinates.coordinates, coordinates.startAngle, coordinates.endAngle);
      ctx.lineTo(coordinates.x,coordinates.y);
      ctx.fillStyle = 'hsl(' + 360 * Math.random() + ', 50%, 50%)';
      ctx.fill();
  });
    return (
        <div className="App" >
          <div className='canvas'>
            <strong>Draw circle in canvas</strong>
          </div>
          <canvas ref={this.canvas} className="plan" width="800" height="500" onClick={this.drawCircle}/>
        </div>
    );
  }

}

export default App;
